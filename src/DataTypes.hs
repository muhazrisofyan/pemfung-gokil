-- | Modelling Playing Cards
-- Examples to introduce data types in Haskell
-- Introduction to Functional Programming 2019.

{-# OPTIONS_GHC -Wincomplete-patterns #-}

{-
This started as a skeleton, the definitions were filled in
during lecture 1B. The last few definitions were added during lecture 2A.
-}

-- | Every card has a suit:  ♠ ♥ ♦ ♣
data Suit = Spades | Hearts | Diamonds | Clubs   deriving (Eq,Show)


data Colour = Red | Black  deriving Show

-- | Each suit has a colour – red or black
colour :: Suit -> Colour
{-
colour Hearts   = Red
colour Diamonds = Red
colour Spades   = Black
colour Clubs    = Black
-}
colour Hearts   = Red
colour Diamonds = Red
colour _        = Black   -- wildcard, don't care pattern


-- | Cards have ranks: 2, 3 .. 10, Jack, Queen, King, Ace
data Rank = Numeric Int | Jack | Queen | King | Ace  deriving (Eq,Ord,Show)

all_ranks :: [Rank]
all_ranks = [Numeric n | n<-[2..10]] ++ [Jack, Queen, King, Ace]

-- Note: the type Rank also allows some values that are not proper ranks
-- like Numeric 30 and Numeric (-5)


-- | Whe